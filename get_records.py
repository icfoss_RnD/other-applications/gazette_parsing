# -*- coding: utf-8 -*-
import sys,csv,re
list_file = sys.argv[1]
input_file = open(list_file,'r')
output_file = open(list_file[0:4]+'_records.csv','w')
csv_writer = csv.writer(output_file)
csv_writer.writerow(['File_name','Page_number','District','Taluk','Village'])
for file_path in input_file:
    file_path=file_path.strip("\n")
    page_number=1
    ocr_txt_file = open(file_path+'.txt','r')
    out_obj = []
    flag = 0
    for line in ocr_txt_file:
        if "[PAGE SEPERATOR]" in line:
            page_number += 1
        if "PARTICULARS OF THE AREA" in line or "DETAILS OF LAND" in line:
            flag = 1
        if flag > 0 and "District—" in line:
#            print "District: "+line[line.find("District")+11:line.find('.')]+" in page "+str(page_number)
            district = re.search("District—((No\. \d*)?[^\.]*)\.",line).group(1)
            print "District: "+district+" in page "+str(page_number)
#            print '\n**********************\n'
            flag+=1
        if flag > 0 and "Taluk—" in line:
#            print "Taluk: "+line[line.find("Taluk")+8:line.find('.')]+" in page "+str(page_number)
            taluk = re.search("Taluk—((No\. \d*)?[^\.]*)\.",line).group(1)
            print "Taluk: "+taluk+" in page "+str(page_number)
#            print '\n**********************\n'
            flag+=1
        if flag > 0 and "Village—" in line:
#            print "Village: "+line[line.find("Village")+10:line.find('.')]+" in page "+str(page_number)
            village = re.search("Village—((No\. \d*)?[^\.]*)\.",line).group(1)
            print "Village: "+village+" in page "+str(page_number)
#            print '\n**********************\n'
            flag+=1
        if flag == 4:
            csv_writer.writerow([file_path,page_number,district,taluk,village])
            flag=0
input_file.close()
output_file.close()
