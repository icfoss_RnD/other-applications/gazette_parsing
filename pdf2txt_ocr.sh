#!/bin/bash
while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "Text read from file: $line"
    convert -density 300 $line -depth 8 -strip -background white -alpha off $line."tiff"
    tesseract -c include_page_breaks=1 -c page_separator="[PAGE SEPERATOR]" $line."tiff" $line
done < "$1"
