# importing required modules
import PyPDF2,sys,os,re,shutil
folder = sys.argv[1]
out = open(folder+'_file_paths.txt','w')
for root, directories, filenames in os.walk(folder):
    for file in filenames:
        if ".pdf" in file:
            pdfFileObj = open(os.path.join(root, file), 'rb') # creating a pdf file object
            pdfReader = PyPDF2.PdfFileReader(pdfFileObj) # creating a pdf reader object
            #print(pdfReader.numPages) # printing number of pages in pdf file
            pageObj = pdfReader.getPage(0) # creating a page object
            page_content = pageObj.extractText() # extracting text from page
            required_text = re.sub(r"\W", "", page_content).lower() #striping and to lower
            #print(required_text)
            pdfFileObj.close() # closing the pdf file object
            if "notifications" in required_text and "declarations" in required_text:
                print " file found: "+os.path.join(root, file)
                out.write(os.path.join(root, file)+"\n")
out.close()
